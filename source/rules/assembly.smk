localrules: assembly_stats
##############
## Assembly ##
##############

rule ananas:
    input:
        R1 = "results/taxmapper/{sample_id}/{sample_id}_R1.cut.trim.filtered.fastq.gz",
        R2 = "results/taxmapper/{sample_id}/{sample_id}_R2.cut.trim.filtered.fastq.gz"
    output:
        "results/assembly/ananas/{sample_id}/final.fa"
    params:
        tmp_dir = "$TMPDIR/ananas/{sample_id}",
        out_dir = "results/assembly/ananas/{sample_id}"
    threads: 4
    resources:
        runtime = lambda wildcards, attempt: attempt*attempt*60
    shell:
        """
        mkdir -p {params.tmp_dir}
        mkdir -p {params.out_dir}
        Ananas -dir fr -i {input.R1},{input.R2} -prefix {wildcards.sample_id} -o {params.tmp_dir} -n {threads}
        rm {params.tmp_dir}/contigs.layout.*
        rm {params.tmp_dir}/scaffolds.layout.iter.*
        mv {params.tmp_dir}/* {params.out_dir}/
        """

rule megahit_unfiltered:
    input:
        R1 = "results/preprocess/{sample_id}_R1.cut.trim.fastq.gz",
        R2 = "results/preprocess/{sample_id}_R2.cut.trim.fastq.gz"
    output:
        fa = "results/assembly/megahit/unfiltered/{sample_id}/final.fa",
        R1 = "results/assembly/megahit/unfiltered/{sample_id}/{sample_id}_R1.fastq.gz",
        R2 = "results/assembly/megahit/unfiltered/{sample_id}/{sample_id}_R2.fastq.gz"
    log: "results/assembly/megahit/unfiltered/{sample_id}/log"
    params:
        min_contig_len = config["megahit_min_contig_len"],
        out_dir = "results/assembly/megahit/unfiltered/{sample_id}",
        tmp_dir = "$TMPDIR/megahit/{sample_id}",
        tmp_dir_base = "$TMPDIR/megahit"
    threads: 10
    resources:
        runtime = lambda wildcards, attempt: attempt*attempt*30
    shell:
        """
        wd=$(pwd)
        # Link input into output directory
        ln -s $wd/{input.R1} $wd/{output.R1}
        ln -s $wd/{input.R2} $wd/{output.R2}
        mkdir -p {params.tmp_dir_base}
        megahit -1 {input.R1} -2 {input.R2} --prune-level 3 \
            --min-contig-len {params.min_contig_len} -o {params.tmp_dir} \
            -t {threads} > {log} 2>&1
        mv {params.tmp_dir}/final.contigs.fa {output.fa}
        mv {params.tmp_dir}/opts.txt {params.out_dir}
        """

rule megahit_filtered:
    input:
        R1 = "results/filtered/{sample_id}/{sample_id}_R1.filtered.union.fastq.gz",
        R2 = "results/filtered/{sample_id}/{sample_id}_R2.filtered.union.fastq.gz"
    output:
        fa = "results/assembly/megahit/filtered/{sample_id}/final.fa",
        R1 = "results/assembly/megahit/filtered/{sample_id}/{sample_id}_R1.fastq.gz",
        R2 = "results/assembly/megahit/filtered/{sample_id}/{sample_id}_R2.fastq.gz"
    log: "results/assembly/megahit/filtered/{sample_id}/log"
    params:
        min_contig_len = config["megahit_min_contig_len"],
        out_dir = "results/assembly/megahit/filtered/{sample_id}",
        tmp_dir = "$TMPDIR/megahit/{sample_id}",
        tmp_dir_base = "$TMPDIR/megahit"
    threads: 10
    resources:
        runtime = lambda wildcards, attempt: attempt*attempt*30
    shell:
        """
        wd=$(pwd)
        # Link input into output directory
        ln -s $wd/{input.R1} $wd/{output.R1}
        ln -s $wd/{input.R2} $wd/{output.R2}
        mkdir -p {params.tmp_dir_base}
        megahit -1 {input.R1} -2 {input.R2} --prune-level 3 \
            --min-contig-len {params.min_contig_len} -o {params.tmp_dir} \
            -t {threads} > {log} 2>&1
        mv {params.tmp_dir}/final.contigs.fa {output.fa}
        mv {params.tmp_dir}/opts.txt {params.out_dir}
        """

rule megahit_taxmapper:
    input:
        R1 = "results/taxmapper/{sample_id}/{sample_id}_R1.cut.trim.filtered.fastq.gz",
        R2 = "results/taxmapper/{sample_id}/{sample_id}_R2.cut.trim.filtered.fastq.gz"
    output:
        fa = "results/assembly/megahit/taxmapper/{sample_id}/final.fa",
        R1 = "results/assembly/megahit/taxmapper/{sample_id}/{sample_id}_R1.fastq.gz",
        R2 = "results/assembly/megahit/taxmapper/{sample_id}/{sample_id}_R2.fastq.gz"
    log: "results/assembly/megahit/taxmapper/{sample_id}/log"
    params:
        min_contig_len = config["megahit_min_contig_len"],
        out_dir = "results/assembly/megahit/taxmapper/{sample_id}",
        tmp_dir = "$TMPDIR/megahit/{sample_id}",
        tmp_dir_base = "$TMPDIR/megahit"
    threads: 10
    resources:
        runtime = lambda wildcards, attempt: attempt*attempt*30
    shell:
        """
        wd=$(pwd)
        # Link input into output directory
        ln -s $wd/{input.R1} $wd/{output.R1}
        ln -s $wd/{input.R2} $wd/{output.R2}
        mkdir -p {params.tmp_dir_base}
        megahit -1 {input.R1} -2 {input.R2} --prune-level 3 \
            --min-contig-len {params.min_contig_len} -o {params.tmp_dir} \
            -t {threads} > {log} 2>&1
        mv {params.tmp_dir}/final.contigs.fa {output.fa}
        mv {params.tmp_dir}/opts.txt {params.out_dir}
        """

rule megahit_bowtie:
    input:
        R1="results/bowtie2/{sample_id}/{sample_id}_R1.fungi.nospruce.fastq.gz",
        R2="results/bowtie2/{sample_id}/{sample_id}_R2.fungi.nospruce.fastq.gz"
    output:
        fa = "results/assembly/megahit/bowtie2/{sample_id}/final.fa",
        R1 = "results/assembly/megahit/bowtie2/{sample_id}/{sample_id}_R1.fastq.gz",
        R2 = "results/assembly/megahit/bowtie2/{sample_id}/{sample_id}_R2.fastq.gz"
    log: "results/assembly/megahit/bowtie2/{sample_id}/log"
    params:
        min_contig_len = config["megahit_min_contig_len"],
        out_dir = "results/assembly/megahit/bowtie2/{sample_id}",
        tmp_dir = "$TMPDIR/megahit/{sample_id}",
        tmp_dir_base = "$TMPDIR/megahit"
    threads: 10
    resources:
        runtime = lambda wildcards, attempt: attempt*attempt*30
    shell:
        """
        wd=$(pwd)
        # Link input into output directory
        ln -s $wd/{input.R1} $wd/{output.R1}
        ln -s $wd/{input.R2} $wd/{output.R2}
        mkdir -p {params.tmp_dir_base}
        megahit -1 {input.R1} -2 {input.R2} --prune-level 3 \
            --min-contig-len {params.min_contig_len} -o {params.tmp_dir} \
            -t {threads} > {log} 2>&1
        mv {params.tmp_dir}/final.contigs.fa {output.fa}
        mv {params.tmp_dir}/opts.txt {params.out_dir}
        """

rule fastuniq_for_assembly:
    input:
        R1 = lambda wildcards: assemblies[wildcards.assembly]["R1"],
        R2 = lambda wildcards: assemblies[wildcards.assembly]["R2"]
    output:
        "results/fastuniq/{assembly}/R1.fastuniq.gz",
        "results/fastuniq/{assembly}/R2.fastuniq.gz"
    params:
        filelist = os.path.expandvars("$TMPDIR/fastuniq/{assembly}/filelist"),
        tmpdir = os.path.expandvars("$TMPDIR/fastuniq/{assembly}"),
        R1 = os.path.expandvars("$TMPDIR/fastuniq/{assembly}/R1.fastuniq"),
        R2 = os.path.expandvars("$TMPDIR/fastuniq/{assembly}/R2.fastuniq")
    resources:
        runtime = lambda wildcards, attempt: attempt**2*60*10,
        mem_mb = lambda wildcards, attempt: attempt**2*128000
    threads: 20
    run:
        shell("mkdir -p {params.tmpdir}")
        files = []
        # Unzip and create filelist for fastuniq
        for i, r1 in enumerate(input.R1):
            basename1 = os.path.basename(r1).rstrip(".gz")
            shell("gunzip -c {r1} > {params.tmpdir}/{basename1}")
            r2 = input.R2[i]
            basename2 = os.path.basename(r2).rstrip(".gz")
            shell("gunzip -c {r2} > {params.tmpdir}/{basename2}")
            files+=[os.path.join(params.tmpdir,basename1),os.path.join(params.tmpdir, basename2)]
        # Write the filelist
        with open(params.filelist, 'w') as fhout:
            fhout.write("{}".format("\n".join(files)))
        shell("fastuniq -i {params.filelist} -o {params.R1} -p {params.R2} -c 1")
        shell("gzip {params.R1}")
        shell("gzip {params.R2}")
        shell("mv {params.R1}.gz {output[0]}")
        shell("mv {params.R2}.gz {output[1]}")
        shell("rm -r {params.tmpdir}")

rule megahit_co:
    input:
        R1 = "results/fastuniq/{assembly}/R1.fastuniq.gz",
        R2 = "results/fastuniq/{assembly}/R2.fastuniq.gz"
    output:
        fa = "results/co-assembly/megahit/{assembly}/final.fa"
    log: "results/co-assembly/megahit/{assembly}/log"
    params:
        min_contig_len = config["megahit_min_contig_len"],
        out_dir = "results/co-assembly/megahit/{assembly}",
        tmp_dir = "$TMPDIR/megahit/{assembly}",
        tmp_dir_base = "$TMPDIR/megahit"
    threads: 20
    resources:
        runtime = lambda wildcards, attempt: attempt**2*60*240
    shell:
        """
        wd=$(pwd)
        mkdir -p {params.tmp_dir_base}
        megahit -1 {input.R1} -2 {input.R2} --prune-level 3 \
            --min-contig-len {params.min_contig_len} -o {params.tmp_dir} \
            -t {threads} > {log} 2>&1
        mv {params.tmp_dir}/final.contigs.fa {output.fa}
        mv {params.tmp_dir}/opts.txt {params.out_dir}
        rm -r {params.tmp_dir}
        """

rule assembly_stats:
    input:
        expand("results/assembly/{{assembler}}/{{source}}/{sample_id}/final.fa",
            sample_id = samples.keys())
    output:
        "results/report/assembly/{source}_{assembler}_stats.tsv",
        "results/report/assembly/{source}_{assembler}_size_dist.tsv"
    run:
        names = [x.split("/")[-2] for x in input]
        shell("python source/utils/assembly_stats.py -i {input} -n {names} --size-dist-file {output[1]} > {output[0]}")

rule co_assembly_stats:
    input:
        "results/co-assembly/megahit/{assembly}/final.fa"
    output:
        "results/report/co-assembly/{assembly}_assembly_stats.tsv",
        "results/report/co-assembly/{assembly}_assembly_size_dist.tsv"
    shell:
        """
        python source/utils/assembly_stats.py -i {input[0]} -n {wildcards.assembly} --size-dist-file {output[1]} > {output[0]}
        """