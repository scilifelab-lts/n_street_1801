#!/usr/bin/env

import pandas as pd
import subprocess
import sys
import os


def validate_samples(samples):
    """
    Performs checks on the samples provided in the sample_file_list

    If the sample file list contains paths to the R1 and R2 files, either:
    1) explicitly via the 'Read_file' and 'Paired_file' columns or
    2) implicitly by having the files present in the datadir (default:
    data/raw) in the format <sample>_{1,2}.fastq.gz
    the workflow proceeds without issue.

    If the files are not given or not present in the datadir, a look-up is done
    on the first SRA accession using sratools. If there are no runs matching
    then it's very likely that the workflow won't have any data to work with.

    :param samples: samples dictionary
    :return:
    """
    for sample, items in samples.items():
        if os.path.exists(samples[sample]["R1"]) and os.path.exists(samples[sample]["R2"]):
            continue
        acc = items['accession']
        cmd = ['sratools', 'info', acc]
        p = subprocess.run(cmd, stdout=subprocess.PIPE).stdout
        found = p.decode().split("\n")[0].split(":")[-1].lstrip()
        if int(found) == 0:
            sys.exit("""
########################## WARNING ###################################
# Accession: {} could not be found. Is it a valid SRA accession?     
#                                                                    #
# If you intend to use locally stored fastq files, make sure your    #
# sample file list contains columns named 'Read_file' and            #
# 'Paired_file' with paths pointing to the corresponding fastq files #
# for each sample.                                                   #
########################## WARNING ###################################
            """.format(acc))
        break


def parse_sample_list(f, config):
    """
    Parse the sample list and set up input file names for the assembly
    based on configuration
    """
    samples = {}
    map_dict = {}
    source = config['read_source']
    suffices = {'unfiltered': 'cut.trim.fastq.gz',
                'filtered': 'filtered.union.fastq.gz',
                'taxmapper': 'cut.trim.filtered.fastq.gz',
                'bowtie2': 'fungi.nospruce.fastq.gz'}
    # Read sample list
    df = pd.read_csv(f, comment='#', header=0, sep='\t', index_col=0, dtype=str)
    df.fillna('', inplace=True)
    # If it's just a one-column file, expand it by assuming that the
    # sra accessions are in the first column
    if df.shape[1] == 0 or 'accession' not in df.columns:
        df = df.assign(accession=pd.Series(df.index, index=df.index))
        df.index.name = "Sample"
    assemblies = {}
    if 'assembly' in df.columns:
        for assembly in df.assembly.unique():
            assemblies[assembly] = {'R1': [], 'R2': []}
    for sample in df.index:
        try:
            R1 = df.loc[sample,'Read_file']
            R2 = df.loc[sample,'Pair_file']
        except KeyError:
            R1 = '{}_1.fastq.gz'.format(sample)
            R2 = '{}_2.fastq.gz'.format(sample)
        if 'accession' in df.columns:
            accession = df.loc[sample, 'accession']
        else:
            accession = ''
        if len(assemblies.keys()) > 0:
            if df.loc[sample,'assembly'] != '':
                assembly = df.loc[sample, 'assembly']
                # Define reads for fastuniq
                R1_f = 'results/{source}/{sample}/{sample}_R1.{suff}'.format(source=source,
                                                                             sample=sample, suff=suffices[source])
                R2_f = 'results/{source}/{sample}/{sample}_R2.{suff}'.format(source=source,
                                                                             sample=sample, suff=suffices[source])
                assemblies[assembly]['R1'].append(R1_f)
                assemblies[assembly]['R2'].append(R2_f)
                map_dict[sample] = {'R1': R1_f, 'R2': R2_f}

        samples[sample]={'R1': '{dir}/{f}'.format(f=R1, dir=config['datadir']),
                         'R2': '{dir}/{f}'.format(f=R2, dir=config['datadir']),
                         'accession': accession}
    # Check if workflow can start on these samples
    validate_samples(samples)
    return samples, map_dict, assemblies
