FROM centos:7.6.1810

LABEL maintainer="John Sundh" email=john.sundh@nbis.se
LABEL description="Analysis workflow image for fungal \
metatranscriptomics used in Schneider et al 2021  mSystems Feb 2021, 6 (1) \
e00884-20; DOI: 10.1128/mSystems.00884-20."

# Use bash as shell
SHELL ["/bin/bash", "-c"]

# Set workdir
WORKDIR /analysis

# Update packages
RUN yum -y update && yum -y install bzip2 wget unzip && yum clean all

# Add conda to PATH and set locale
ENV PATH="/opt/miniconda3/bin:${PATH}"
# Add conda environment to path
ENV PATH="/opt/miniconda3/envs/workflow/bin:${PATH}"
ENV LC_ALL en_US.UTF-8
ENV LC_LANG en_US.UTF-8

# Install miniconda
RUN wget -O Miniconda3-4.6.14-Linux-x86_64.sh https://repo.continuum.io/miniconda/Miniconda3-4.6.14-Linux-x86_64.sh && \
    bash Miniconda3-4.6.14-Linux-x86_64.sh -bf -p /opt/miniconda3/ && \
    rm Miniconda3-4.6.14-Linux-x86_64.sh && \
    ln -s /opt/miniconda3/etc/profile.d/conda.sh /etc/profile.d/conda.sh && \
    /opt/miniconda3/bin/conda clean -tipsy && \
    echo ". /opt/miniconda3/etc/profile.d/conda.sh " >> ~/.bashrc && \
    echo "conda activate workflow" >> ~/.bashrc

# Set TMPDIR variable
ENV TMPDIR="/tmp"

# Add environment file
COPY environment.yml .
# Install environment
RUN conda env create -f environment.yml -n workflow && conda clean -tipsy

# Install GeneMarkS software
RUN mkdir /tmpdir
COPY source/external/gmst_linux_64.tar.gz /tmpdir/
RUN tar -C /tmpdir/ -xf /tmpdir/gmst_linux_64.tar.gz && \
    cp /tmpdir/Gibbs3 /opt/miniconda3/envs/workflow/bin && \
    cp /tmpdir/gmhmmp /opt/miniconda3/envs/workflow/bin && \
    cp /tmpdir/gmst.pl /opt/miniconda3/envs/workflow/bin && \
    cp /tmpdir/probuild /opt/miniconda3/envs/workflow/bin && \
    rm -rf /tmpdir

# Install workflow
RUN mkdir -p config source/external source/rules source/utils envs
COPY config ./config/
COPY source/rules ./source/rules/
COPY source/utils ./source/utils/
COPY source/external/hmmscan-parser.sh ./source/external/
COPY envs/emapper.yaml envs/blobtools.yaml ./envs/
COPY Snakefile ./
COPY config.yml ./
COPY sample_list.tsv ./

CMD snakemake -j 8 --use-conda -p