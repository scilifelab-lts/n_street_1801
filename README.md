# Functional insights into the impacts of nitrogen fertilisation on forest belowground fungal metacommunities

This is the Bitbucket repository for the N_Street_1801 (Functional 
insights into the impacts of nitrogen fertilisation on forest belowground
 fungal metacommunities) project.
 
# NEW location

This workflow has [moved to GitHub](https://github.com/NBISweden/fungal-trans) and will be actively maintained there.

## Installation
To install this workflow you can either:

1. Clone the **BitBucket** repository and install dependencies, or
2. Pull and run the workflow image with either **Docker** or **Singularity**

### OPTION 1 (git clone)
#### 1.1 Cloning this repository
Make sure [git](https://git-scm.com/) is installed on your computer.
Then clone this repository by typing in a terminal:

```
git clone https://bitbucket.org/scilifelab-lts/n_street_1801.git
```

#### 1.2 Install dependencies
Nearly all dependencies are handled using [conda](https://docs.conda.io/en/latest/miniconda.html).
To set up the software environment type:

```
conda env create -f environment.yml -n workflow
```

After everything is finished you can activate the environment with:

```
conda activate workflow
```

##### 1.2.1 Install GeneMarkS-T gene caller
The [GeneMarkS-T](http://exon.gatech.edu/GeneMark/) gene caller software used in this workflow can be 
downloaded for Linux from [this download page](http://topaz.gatech.edu/GeneMark/license_download.cgi).
Activate the workflow environment created in the step above, then follow the 
instructions to install GeneMarkS-T into the activated environment. 
**Alternatively you can use the Docker image (see below) which comes with GeneMarkS-T
pre-installed.**

Once you've performed these steps you can run the workflow with:
```
snakemake
```

### OPTION 2 (Docker/Singularity)
You can also run this workflow using the Docker or Singularity containers.

#### Docker
To run the workflow with Docker do:

```
docker pull natstreetlab/funtrans:latest
docker run --rm -it natstreetlab/funtrans:latest /bin/bash
```

This will give you a terminal prompt inside the workflow container. The 
workflow is now available to run with the command:

```
snakemake -j 8 --use-conda -rpk
```

#### Singularity
To run the workflow with Singularity do:

```
singularity pull johnne/default/funtrans 
```
This will pull the workflow container into the file `funtrans_latest.sif`.

Next you can run an interactive shell in the container with:

```
singularity shell funtrans_latest.sif
```

## Running the workflow
The workflow requires a list of samples to use as input. By default the 
workflow looks for a file called `sample_list.tsv` in the working directory
 but you can name it whatever you like and specify the path with 
 `snakemake --config sample_file_list=<path-to-your-samplefile>`. Data for these
samples can either be present on disk or in a remote sequence read archive.

### Option1: Raw data on disk
Let's say you have your raw data downloaded and stored in a directory
called `/home/data/raw` and your study contains 3 samples called 
sample1, sample2 and sample3. Let's also say you want to make a co-assembly
of samples 2 and 3 and assemble sample 1 separately. Then your **tab-separated**
 sample file list should look like this:  

| sample | Read_file | Pair_file | assembly |
|--------|-----------|-----------|----------|
|sample1|sample1_1.fastq.gz|sample1_2.fastq.gz|assembly1|
|sample2|sample2_1.fastq.gz|sample2_2.fastq.gz|assembly2|
|sample3|sample3_1.fastq.gz|sample3_2.fastq.gz|assembly2|

In this example, we'll name sample file list `sample_list.tsv`. You can
then run the workflow for these samples with:
```
snakemake -p
```

To make 
the raw data files and the sample file list available to the Docker 
container we can run:

```
docker run --rm -it -v /home/data/raw:/analysis/data/raw -v $(pwd)/sample_list.tsv:/analysis/sample_list.tsv funtrans:base /bin/bash
```
This command mounts the `/home/data/raw` folder inside the container in
 `/analysis/data/raw` and the `sample_list.tsv` file in `/analysis/sample_list.tsv`. 

By default the workflow looks for a file called `sample_list.tsv` and 
for sequence files under `data/raw` in the working directory. By running
the command above you could directly run the workflow inside the container
by typing:

```
snakemake
```

See below for more information on how to **configure** and **run** the
workflow.

###### Option2: Data in a sequence read archive

#### Singularity

```
singularity pull library://johnne/default/funtrans
```

### Installing the SLURM snakemake profile
To run this workflow in a cluster environment (such as Uppmax) you
should install the [SLURM profile](https://github.com/Snakemake-Profiles/slurm)
for Snakemake.

Install the cookiecutter environment:
```
mkdir envs/cookiecutter
conda env create -f envs/cookiecutter.yaml -p envs/cookiecutter
```

Activate the environment and deploy the profile:
```
conda activate envs/cookiecutter
mkdir profiles
cookiecutter https://github.com/Snakemake-Profiles/slurm.git -o profiles/
```
When prompted, enter the required info as seen below:
```
account []: snic2018-8-230
error []:
output []:
partition []: core
profile_name [slurm]: slurm
Select submit_script:
1 - slurm-submit.py
2 - slurm-submit-advanced.py
Choose from 1, 2 (1, 2) [1]: 2
```
Now you can run the workflow on Uppmax with
`snakemake --profile profiles/slurm` which will submit and monitor jobs
on the cluster.

## Running the workflow
First activate the environment under envs/n_street
```
conda activate envs/n_street
```

By default the workflow uses the configuration file `config/config.yaml`.
You can either make changes in that config file or copy the file and make
your changes in the copy. To run the workflow with another config file
specify `--configfile <yourconfig>` on the snakemake command line.

### Running the full workflow
To get an overview of the jobs that will be executed do:
```
snakemake --profile profiles/slurm -j 100 -np
```
`-n` executes a 'dry-run' that doesn't actually do anything and instead
the jobs to be run are printed to stdout. The `-j 100` parameter tells
snakemake to have at most 100 jobs in the queue at the same time.

The workflow is divided into steps that you can run separately. Below
are descriptions of these targets, their output and how to run them.

#### Preprocessing
Preprocessing of reads includes adapter trimming with [Cutadapt](https://github.com/marcelm/cutadapt),
followed by quality trimming with [Trimmomatic](http://www.usadellab.org/cms/?page=trimmomatic).
Config parameters relevant to this stage are: the `qc_trim_settings`.
To run preprocessing only, type:

```
snakemake --profile profiles/slurm -j 100 -p preprocess
```

A MultiQC report file is generated under
`results/report/preprocess/preprocess_report.html`

#### Filtering
Preprocessed reads are filtered to extract fungi-only reads. This can
be done in three ways: using bowtie2, taxmapper or a union of these
 methods
##### Taxmapper filtering
Here reads are classified taxonomically using [Taxmapper](https://bitbucket.org/dbeisser/taxmapper/)
([Beisser et al 2017](https://bmcgenomics.biomedcentral.com/articles/10.1186/s12864-017-4168-6)).
In this workflow, taxmapper is run with a probability threshold setting
`-a 0.25` and the best hit for a pair is retained for each read.

To run this step type:
```
snakemake --profile profiles/slurm -j 100 -p taxmapper_filter
```

Output from this step:
```
results/taxmapper/<sample_id>/
    - <sample_id>_R{1,2}.cut.trim.filtered.fastq.gz : Fastq files with reads identified as fungi
    - taxa_counts_level1.tsv : Read counts of taxa summarized at level 1
    - taxa_counts_level2.tsv : Read counts of taxa summarized at level 2
    - taxa_filtered.tsv.gz : Filtered read assignments from taxmapper filter
    - taxa_identities.tsv : Average identities for different taxa.
    - taxa.tsv.gz : Raw read assignments from taxmapper map
```

In addition, results are summarized across samples and stored under `results/report/taxmapper`.


##### Bowtie2 filtering
Here reads are aligned to a fungi transcript dataset (retaining reads
that align concordantly) followed by aligning to a set of spruce gene
sequences (retaining reads that **do not** align concordantly).

To run this step type:
```
snakemake --profile profiles/slurm -j 100 -p bowtie_filter
```

Output from this step:
```
results/bowtie2/<sample_id>/
    - <sample_id>.fungi.bam : The bam file from the fungi alignment step
    - <sample_id>_R{1,2}.fungi.fastq.gz : The fastq files with reads aligning concordantly to fungi
    - <sample_id>_R{1,2}.fungi.nospruce.fastq.gz : The fastq files with reads aligning concordantly to fungi and not to spruce
    - <sample_id>_R1.fungi.spruce.fastq.gz : The fastq files with reads aligning concordantly to fungi AND to spruce
    - <sample_id>.spruce.bam : The bam file from aligning the fungi reads to spruce
```

In addition, results are summarized across samples and stored in `results/report/filtering/bowtie2_filter_report.html`

##### Union filtering
Here reads are combined from the taxmapper and bowtie2 filtering steps.
All reads that align concordantly to the fungi dataset but not to spruce
(using bowtie2) **as well as** reads that are classified as fungal using
taxmapper.

To run this step, type:
```
snakemake --profile profiles/slurm -j 100 -p filter
```

#### Assembly
Assembly is done with either [Megahit](https://github.com/voutcn/megahit)
 or [Ananas](https://github.com/AnanasAssembler/AnanasAssembler). The
 assembler is set in the config file `config/config.yaml` by setting
 `megahit: True` and/or `ananas: True`.

 By default, the workflow only uses Megahit (i.e. `megahit: True, ananas: False`)

 Assembly depends on the filtering step(s) chosen. By default, the
 workflow uses the union of reads from bowtie2 and taxmapper filtering
 and these are then assembled using Megahit.

 To run assembly with default settings, type:
 ```
 snakemake --profile profiles/slurm -j 100 -p assemble
 ```

## Post-analysis with jupyter notebook

For the 2020 'Technical paper' results produced during the workflow run were
used to analyse and produce figures. The [Technical_paper.ipynb](source/jupyter/Technical_paper.ipynb)
notebook was used here. It requires files listed in []
